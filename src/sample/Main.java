package sample;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.imaging.tiff.TiffDataFormat;
import com.drew.metadata.Metadata;
import com.drew.metadata.Directory;
import com.drew.metadata.Tag;
import com.drew.metadata.exif.ExifIFD0Directory;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import javafx.application.Application;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javafx.scene.control.TextArea;
import javafx.scene.control.Button;


import java.awt.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

public class Main extends Application {

    private Desktop desktop = Desktop.getDesktop();
    public static final String JPG = ".jpg";
    public static final String MP4 = ".mp4";

    @Override
    public void start(final Stage primaryStage) throws Exception{
        final FileChooser fileChooser = new FileChooser();
        final DirectoryChooser directoryChooser = new DirectoryChooser();
        final TextArea textArea = new TextArea();
        textArea.setMinHeight(600);
        textArea.setEditable(false);
        Button btnOpenPhotoFolder = new Button("Метаданные по 1му файлу");
        Button btnUpdatePhotodata = new Button("Обновить дату создания *.jpg файлов");
        Button btnUpdateVideodata = new Button("Обновить дату создания *.mp4 файлов (WIP)");
        //TODO Обновление mp4 файлов и видеофайлов другого формата
        Button btnCountFiles = new Button("Проверить папку *.jpg");

        btnOpenPhotoFolder.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                textArea.clear();
                List<File> files = fileChooser.showOpenMultipleDialog(primaryStage);
                printMetaData(textArea, "В процессе, ожидай");
                printMetaData(textArea, getMetaData(files));
            }
        });

        btnCountFiles.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                textArea.clear();
                File directory = directoryChooser.showDialog(primaryStage);
                printMetaData(textArea, "В процессе, ожидай");
                printMetaData(textArea, countFilesInFolder(directory));
            }
        });

        btnUpdatePhotodata.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                textArea.clear();
                File directory = directoryChooser.showDialog(primaryStage);
                printMetaData(textArea, "В процессе, ожидай");
                printMetaData(textArea, updateDateModifiedWithExifFD0(directory, JPG));
            }
        });

        btnUpdateVideodata.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                textArea.clear();
                File directory = directoryChooser.showDialog(primaryStage);
                printMetaData(textArea, "В процессе, ожидай");
                printMetaData(textArea, updateDateModifiedWithExifFD0(directory, MP4));
            }
        });

        //TODO Статус обновления выводить в процессе обновления, а не в самом конце. Если файлов много сейчас не будет никаких сообщений

        VBox root = new VBox();
        root.setSpacing(10);
        root.setMinHeight(250);
        root.getChildren().addAll(btnOpenPhotoFolder, btnCountFiles, btnUpdatePhotodata, btnUpdateVideodata, textArea);


//        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Hello World");
        Scene scene = new Scene(root, 1000, 800);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private String countFilesInFolder(File directory) {
        String filePath;
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);

        File[] imageFileList = getFilteredFilesFromDir(directory, JPG);

        printWriter.println(countAllFilesInPath(directory));
        printWriter.println();
        printWriter.println("В том числе изображения:");

        int countImages = 0;
        for (File file : imageFileList) {
            countImages++;
            printWriter.println(file.getAbsolutePath());
        }

        printWriter.println();
        printWriter.println("Всего изображений - " + countImages);
        return stringWriter.toString();
    }

    private String updateDateModifiedWithExifFD0(File directory, String format) {
        String filePath;
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
        int countFilesToUpdate = 0;
        int countUpdateSuccessFiles = 0;
        int countUpdateFailFiles = 0;
        File[] imageFileList = getFilteredFilesFromDir(directory, format);

        for (File file : imageFileList) {
            countFilesToUpdate++;
            filePath = file.getAbsolutePath();
            printWriter.println("Файл - " + filePath);
            printWriter.println("Текущая дата создания - " + sdf.format(file.lastModified()));

            try {
                Metadata metadata = ImageMetadataReader.readMetadata(file);
                Directory dir = metadata.getFirstDirectoryOfType(ExifIFD0Directory.class);

                if (dir != null) {
                    Date metaDatePhotoCreated = dir.getDate(ExifIFD0Directory.TAG_DATETIME);

                    if (metaDatePhotoCreated != null) {
                        countUpdateSuccessFiles++;
                        printWriter.println("Фото сделано - " + metaDatePhotoCreated);
                        printWriter.println("Обновляю дату создания...");

                        if (file.setLastModified(Math.abs(metaDatePhotoCreated.getTime()))) {
                            printWriter.println("Дата создания файла обновлена. Текущая дата: " + sdf.format(file.lastModified()));
                            printWriter.println();
                        } else {
                            printWriter.println("Не удалось обновить дату");
                        }
                    } else {
                        countUpdateFailFiles++;
                        printWriter.println();
                        printWriter.println("Ошибка. У файла" + "\n" + filePath + "\n" + "Нет TAG_DATETIME метаданных. Обновление не удалось :(");
                        printWriter.println();
                    }
                } else {
                    countUpdateFailFiles++;
                    printWriter.println();
                    printWriter.println("Ошибка. У файла" + "\n" + filePath + "\n" + ". ExifIFD0Directory метаданные не найден. Обновление не удалось :(");
                    printWriter.println();
                }
            } catch (ImageProcessingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
                return e.getMessage();
            }
        }
        printWriter.println();
        printWriter.println("Всего файлов к обновлению - " + countFilesToUpdate);
        printWriter.println("Обновлено успешно - " + countUpdateSuccessFiles);
        printWriter.println("Ошибка при обновлении - " + countUpdateFailFiles);
        return stringWriter.toString();
    }

    private String countAllFilesInPath(File directory) {
        int countFiles = 0;
        if (directory.listFiles() != null) {
            File[] files = directory.listFiles();
            for (File file : files) {
                countFiles++;
            }
            return "Найдено " + countFiles + " файлов";
        } else {
            return "Папка пустая";
        }
    }

    private File[] getFilteredFilesFromDir(File directory, String format) {

        if (format.equals(JPG)) {
            FilenameFilter imageFileFilter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    String lowercaseName = name.toLowerCase();
                    if (lowercaseName.endsWith(".jpg")) {
                        return true;
                    } else {
                        return false;
                    }
                }
            };
            return directory.listFiles(imageFileFilter);
        } else if (format.equals(MP4)) {
            FilenameFilter videoFileFilter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    String lowercaseName = name.toLowerCase();
                    if (lowercaseName.endsWith(".mp4")) {
                        return true;
                    } else {
                        return false;
                    }
                }
            };
            return directory.listFiles(videoFileFilter);
        } else {
            return null;
        }
    }

    private void printMetaData(TextArea textArea, String text) {
        textArea.appendText(text + "\n");
    }

    private String getMetaData(List<File> files) {
        String filePath;
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);

        for (File file : files) {
            filePath = file.getAbsolutePath();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
            printWriter.println(sdf.format(file.lastModified()));
            printWriter.println(filePath);

            try {
                Metadata metadata = ImageMetadataReader.readMetadata(file);
                Iterable<Directory> iterable = metadata.getDirectories();
                int iterDirs = 0;

                for (Directory dr : iterable) {
                    iterDirs++;
                    Collection<Tag> tags = dr.getTags();
                    int iterTags = 0;
                    printWriter.println();
                    printWriter.println(dr.getName());
                    for (Tag tag : tags) {
                        iterTags++;
                        printWriter.println(iterDirs + "." + iterTags + ": " + tag.getDirectoryName() + ": " + tag.getTagTypeHex() + tag.getTagName() + ", " + tag.getDescription());
                    }
                }
                printWriter.println();
                return stringWriter.toString();
            } catch (ImageProcessingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return stringWriter.toString();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
